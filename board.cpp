// Chau-Nhi Phan & Aemile Donoghue
// Section 05 
// board.cpp - implementation for Board class
 
#include<iostream>
#include<cstdlib>
#include<unistd.h>
using namespace std;
#include "gfxnew.h"
#include"board.h"

// constructor
Board::Board(){

  dots = 272; // total number of dots on board
  over = false;

  // initialize dots and walls
  for (int i = 0; i < 31; i++){
    for (int j = 0; j < 29; j++){
      board[i][j] = '.';
    }
  }
  for (int k = 0; k < 29; k++){
    board[0][k] = '_';
    board[30][k] = '_';
  }
  for (int k = 1; k < 31; k++){
    if (k != 14){
      board[k][0] = '|';
      board[k][28] = '|';
    }
  }
  
  // Block off Space for dome
  for (int i = 12; i < 17; i++){
    for (int j = 10; j < 19; j++){
      board[i][j] = '&';
    }
  }

  for(int j = 9; j < 20; j++){
    board[11][j] = ' ';
    board[17][j] = ' ';
  }
  for (int i = 11; i < 18; i++){
    board[i][9] = ' ';
    board[i][19] = ' ';
  }

  // Walls
  for (int i = 2; i < 5; i++){
    for (int n = 9; n < 13; n++){
      setStatus(i,14+n,'-');
      setStatus(i,14-n,'-');
      setStatus(6,14+n,'-');
      setStatus(6,14-n,'-');
      setStatus(7,14+n,'-');
      setStatus(7,14-n,'-');
      setStatus(21,14+n,'-');
      setStatus(21,14-n,'-');
      setStatus(22,14+n,'-');
      setStatus(22,14-n,'-');
    }
    for (int n = 3; n < 8; n++){
      setStatus(i,14+n,'-');
      setStatus(i,14-n,'-');
      setStatus(21,14+n,'-');
      setStatus(21,14-n,'-');
      setStatus(22,14+n,'-');
      setStatus(22,14-n,'-');
      for (int j = 9; j < 15; j++){
        setStatus(14+n-2,14+j,'-');
        setStatus(14-(n-2),14+j,'-');
        setStatus(14+n-2,14-j,'-');
        setStatus(14-(n-2),14-j,'-');
      }
    }
    for (int n = 13; n < 16; n++){
      setStatus(i,n,'-');
      setStatus(9,14+n-10,'-');
      setStatus(10,14+n-10,'-');
      setStatus(9,14-(n-10),'-');
      setStatus(10,14-(n-10),'-');
      for (int j = 1; j < 5; j++){
        setStatus(j,n,'-');
        setStatus(5+j,n,'-');
        setStatus(17+j,n,'-');
        setStatus(23+j,n,'-');
      } 
      setStatus(10,n,'-');
      setStatus(22,n,'-');
      setStatus(28,n,'-');
    }
  }
  for (int n = 10; n < 19; n++){
    setStatus(6,n,'-');
    setStatus(7,n,'-');
    setStatus(18,n,'-');
    setStatus(19,n,'-');
    setStatus(24,n,'-');
    setStatus(25,n,'-');
  }
  for (int i = 6; i < 8; i++){
    for (int j = 6; j < 14; j++){
      setStatus(j,14+i,'-');
      setStatus(j,14-i,'-');
    }
    for (int j = 15; j < 20; j++){
      setStatus(j,14+i,'-');
      setStatus(j,14-i,'-');
    }
    for (int j = 24; j < 29; j++){
      setStatus(j,14+i,'-');
      setStatus(j,14-i,'-');
    }
    for (int j = 3; j < 13; j++){
      setStatus(21+i,14+j,'-');
      setStatus(21+i,14-j,'-');
    }
    for (int j = 23; j < 26; j++){
      setStatus(j,14+i+3,'-');
      setStatus(j, 14-(i+3),'-');
    }
    for (int j = 24; j < 26; j++){
      setStatus(j,14+i+6,'-');
      setStatus(j,14-(i+6),'-');
    }
  }

  // Immunity Dots
  setStatus(3,1,'I');  
  setStatus(3,27,'I');  
  setStatus(23,1,'I');  
  setStatus(23,27,'I'); 
  imCount = 55; 

  // GRAPHICS
  drawMaze();
  drawDome();

  // Pacman's starting position
  pacrow = 23; pastpacr = 23;
  paccol = 14; pastpacc = 14;
  setPac();

  // Ghosts
  grow[0] = 10;
  gcol[0] = 12;
  past[0] = true;
  setG(1);
  grow[1] = 15;
  gcol[1] = 13;
  past[1] = true;
  setG(2);
  grow[2] = 15;
  gcol[2] = 14;
  past[2] = true;
  setG(3);
  grow[3] = 15;
  gcol[3] = 15;
  past[3] = true;
  setG(4);
}
 
Board::~Board() {}

//moves pacman to that position 
bool Board::move(char arrow) {
  int row2 = pacrow, col2 = paccol, row1 = pacrow, col1=paccol;
  bool temp = true;
  switch(arrow){
    // adjust point based on arrow pressed
    case 'R': // up arrow
      row2--;
      break;
    case 'Q': // left arrow
      col2--;
      if (col2 < 0)
        col2 = 28;
      break;
    case 'T': // down arrow
      row2++;
      break;
    case 'S': // right arrow
      col2++;
      if (col2 > 28)
        col2 = 0;
      break;
  }
  switch(getStatus(row2, col2)) {
    case ' ': // move pacman there
      pastpacr = pacrow; pastpacc = paccol;
      pacrow = row2; paccol = col2;
      setPac(); // set pacman to be at that position
      setStatus(row1, col1, ' ');
      return true;
      break;
    case 'I': // do everything as normal but set immunity to true first (NO BREAK)
      setImmunity(true);
      imCount += 50;
    case '.': // move pacman there and remove the dot there
      pastpacr = pacrow; pastpacc = paccol;
      pacrow = row2; paccol = col2;
      setPac();
      setStatus(row1, col1, ' ');
      dots--;
      return true;
    case '|': // pacman can't move there so don't do anything
    case '_': // pacman can't move there so don't do anything
    case '-': // pacman can't move there so don't do anything
      temp = false;
      return false;
      break;
    case 'X': // ghost
      pastpacr = pacrow; pastpacc = paccol;
      pacrow = row2; paccol = col2;
      setPac();
      setStatus(row1, col1, ' ');
      return true;
      break;
  }
}

bool Board::moveG(int n, int g) {
  int nrow = grow[g-1], ncol = gcol[g-1];
  switch(n){
    case 1: // up arrow
      nrow--;
      break;
    case 2: // left arrow
      ncol--;
      if (ncol < 0) ncol = 28;
      break;
    case 3: // down arrow
      nrow++;
      break;
    case 4: // right arrow
      ncol++;
      if (ncol > 28) ncol = 0;
      break;
  }
  switch(getStatus(nrow, ncol)){
    case ' ': // move ghost
      gfx_color(0,0,0);
      gfx_fill_rectangle(gcol[g-1]*26,grow[g-1]*26,26,26);
      if (past[g-1]){
        gfx_color(252,179,63);
        gfx_fill_rectangle(26*gcol[g-1]+10,26*grow[g-1]+10, 6, 6);
        setStatus(grow[g-1],gcol[g-1],'.');
      }
      else {
        setStatus(grow[g-1],gcol[g-1],' ');
      }
      grow[g-1] = nrow; gcol[g-1] = ncol;
      setG(g);
      past[g-1] = false;
      return true;
      break;
    case 'I':
      gfx_color(0,0,0);
      gfx_fill_rectangle(gcol[g-1]*26,grow[g-1]*26,26,26);
      if (past[g-1]){
        gfx_color(255,255,255);
        gfx_fill_circle(gcol[g-1]*26+13,grow[g-1]*26+13,8);
        setStatus(grow[g-1],gcol[g-1],'.');
      }
      else {
        setStatus(grow[g-1],gcol[g-1],' ');
      }
      grow[g-1] = nrow; gcol[g-1] = ncol;
      setG(g);
      past[g-1] = true;
      return true;
      break;
    case '.': // move ghost
      gfx_color(0,0,0);
      gfx_fill_rectangle(gcol[g-1]*26,grow[g-1]*26,26,26);
      if (past[g-1]){
        gfx_color(252,179,63);
        gfx_fill_rectangle(26*gcol[g-1]+10,26*grow[g-1]+10, 6, 6);
        setStatus(grow[g-1],gcol[g-1],'.');
      }
      else {
        setStatus(grow[g-1],gcol[g-1],' ');
      }
      grow[g-1] = nrow; gcol[g-1] = ncol;
      setG(g);
      past[g-1] = true;
      return true;
      break;
    case '|': 
    case '_': 
    case '-': 
      return false;
      break;
    case 'O': // pacman
      grow[g-1] = nrow; gcol[g-1] = ncol;
      setG(g);
      break;
  }      
}
      
char Board::getStatus(int row, int col) {
  return board[row][col];
}

// sets the given position to something based on the third int passed to it (right now only sets it to pacman)
void Board::setStatus(int row, int col, char c){
  if (c == 'O') { pacrow = row; paccol = col; setPac();} // move pacman to that position
  else {
    board[row][col] = c;
  }
}

void Board::print() {
  for (int i = 0; i < 31; i++){
    for (int j = 0; j < 29; j++){
      cout << board[i][j];
    }
    cout << endl;
  }
}

// sets board to 'O' at pacman's position
void Board::setPac() {
  board[pacrow][paccol] = 'O';
}

void Board::setG(int color) {
  board[grow[color-1]][gcol[color-1]] = 'X';
  int xc = (gcol[color-1])*(26);
  int yc = (grow[color-1])*(26);
  int r, g, b;
    // for drawing different colors
    switch(color){
      case 1: // red ghost
        r = 255; g = 0; b = 0;
        break;
      case 2: // blue ghost
        r = 102; g = 255; b = 255;
        break;
      case 3: // orange ghost
        r = 255; g = 153; b = 51;
        break;
      case 4: // pink ghost
        r = 255; g = 204; b = 229;
        break;
    }
    gfx_color(r,g,b);
    gfx_fill_arc(xc+3, yc, 23, 23, 0, 180); // body
    gfx_fill_rectangle(xc+3, yc+10, 23, 9);
    short int x1 = xc + 3, x2 = xc + 9, x3 = xc + 19, x4 = xc + 26, x5 = xc + 13;
    short int y1 = yc + 17, y2 = yc + 25;
    XPoint left[] = { {x1, y1}, {x1, y2}, {x2, y1} };
    XPoint middle[] = { {x2, y1}, {x3, y1}, {x5, y2} };
    XPoint right[] = { {x3, y1}, {x4, y1}, {x4, y2} };
    int size = sizeof(left)/sizeof(XPoint);
    gfx_fill_polygon(left, size);
    gfx_fill_polygon(middle, size);
    gfx_fill_polygon(right, size);
    gfx_color(255,255,255); // eyes
    gfx_fill_circle(xc+9,yc+13,4);
    gfx_fill_circle(xc+20,yc+13,4);
    gfx_color(0,0,153);
    gfx_fill_circle(xc+9,yc+14,2);
    gfx_fill_circle(xc+21,yc+14,2);
}

int Board::getPacX() {
  return paccol*26;
}

int Board::getPacY() {
  return pacrow*26;
}

int Board::getPastPacX() {
  return pastpacc*26;
}

int Board::getPastPacY() {
  return pastpacr*26;
}

void Board::setPastPacX() {
  pastpacc = paccol;
}

void Board::setPastPacY() {
  pastpacr = pacrow;
}

void Board::setPast(int g, bool p) {
  past[g-1] = p;
}

int Board::getDots() {
  return dots;
}

void Board::drawMaze() {
  bool statusline;
  // DRAW MAZES
  gfx_color(0,0,255);
  for (int p = 0; p < 31; p++){
    for (int q = 0; q < 29; q++){
      if ((getStatus(p,q) == '|') || (getStatus(p,q) == '-') || (getStatus(p,q) == '_')){
        gfx_color(0,0,255);
        gfx_fill_rectangle(26*q,26*p, 26, 26);
      }
      if (getStatus(p,q) == '.'){
        gfx_color(252,179,63);
        gfx_fill_rectangle(26*q+10,26*p+10, 6, 6);
      }
      if (getStatus(p,q) == 'I'){
        gfx_color(255,255,255);
        gfx_fill_circle(26*q+13,26*p+13,8);
      }
    }
  }
}

void Board::drawDome() {
  // DRAW DOME
  int xcdome = 260;
  int ycdome = 312+13;
  gfx_color(204, 204, 0);
  gfx_fill_arc(xcdome+91, ycdome+5, 50, 70, 0, 180);
  gfx_fill_arc(xcdome+19+91, ycdome-27+15, 12, 20, 0, 360);
  gfx_color(153,100,0);
  gfx_fill_rectangle(xcdome+1+91, ycdome+39, 49, 30);
  gfx_fill_rectangle(299-13, 377, 156+26, 65);
  gfx_color(0,0,0);
  gfx_fill_rectangle(312, 390, 134, 52);
  gfx_color(102,51,0);
  gfx_fill_rectangle(xcdome+1+91, ycdome+39, 49, 5);
  gfx_line(xcdome+16+91, ycdome+40, xcdome+16+91, 389);
  gfx_line(xcdome+35+91, ycdome+40, xcdome+35+91, 389);
  gfx_line(xcdome+91, ycdome+40, xcdome+91, 389);
  gfx_line(xcdome+49+91, ycdome+40, xcdome+49+91, 389);
  XPoint x[] = { {377, 370} , {351, 390} , {400, 390}, {377, 370}};
  int size = sizeof(x)/sizeof(XPoint);
  gfx_fill_polygon(x, size);
}

void Board::collision(int c){ // method to determine if a collision occurred
  if(c != 1) { // check what things you are comparing positions of
    checkBothGhosts(c,1); // if they are both ghosts, their directions will be changed in checkBothGhosts
      // if they are not both ghosts, eat is called within checkBothGhosts so you don't have to do anything here
  }
  if(c != 2) { // check if at same position as g2
    checkBothGhosts(c,2);
  }
  if(c != 3) { // check if at same position as g3
    checkBothGhosts(c,3);
  }
  if(c != 4) { // check if at same position as g4
    checkBothGhosts(c,4);
  }
  if(c != 5){ // check if at same position as pacman (you know they are not both ghosts, but eat is called in checkBothGhosts so you have to call it anyways)
    checkBothGhosts(c,5);
  }
}

void Board::eat(int c, int d){
  // check whether pacman is in immunity mode or not
  if (getImmunity()){ 
    // if pacman is immune, the ghost should get eaten and sent back to the dome
    if (c == 5) { // c is pacman and d is ghost, send d back dome
      sendDome(d);
      dots--;
    }
    else {
      sendDome(c); // c is the ghost and d is pacman, so send c to the dome  
      dots--;
    }
  }
  else { // pacman is not in immuity mode and gets eaten
    // CODE TO END GAME
    over = true;
  }
}

void Board::checkBothGhosts(int c, int d){ // check if both are ghosts
  if ((getGX(c) == paccol) && (getGY(c) == pacrow)) {
    eat(c, d);
  }
}

void Board::sendDome(int c){
  switch(c){
    case 1: // send 1st ghost back dome  setGX(col,ghost)
      gfx_color(0,0,0);
      gfx_fill_rectangle(gcol[c-1]*26,grow[c-1]*26,26,26);
      setGpos(15,16,1);
      setG(1);
      break;
    case 2: // send 2nd ghost back dome
      gfx_color(0,0,0);
      gfx_fill_rectangle(gcol[c-1]*26,grow[c-1]*26,26,26);
      setGpos(15,13,2);
      setG(2);
      break;
    case 3: // send 3rd ghost back dome
      gfx_color(0,0,0);
      gfx_fill_rectangle(gcol[c-1]*26,grow[c-1]*26,26,26);
      setGpos(15,14,3);
      setG(3);
      break;
    case 4: // send 4th ghost back dome
      gfx_color(0,0,0);
      gfx_fill_rectangle(gcol[c-1]*26,grow[c-1]*26,26,26);
      setGpos(15,15,4);
      setG(4);
      break;
  }
}

bool Board::getImmunity(){
  return immunity;
}

void Board::setImmunity(bool im){
  immunity = im;
}

void Board::setGpos(int row, int col, int ghost){
  gcol[ghost-1] = col;
  grow[ghost-1] = row;
}
int Board::getGX(int ghost){
  return gcol[ghost-1];
}

int Board::getGY(int ghost){
  return grow[ghost-1];
}

int Board::getDomeX(int g){
  return domeX[g-1];
}

int Board::getDomeY(){
  return domeY;
}

void Board::resetIm(){
  if (imCount > 0 && immunity) imCount--;
  else if (imCount == 0) {
    immunity = false;
    imCount = 50;
  }
}

int Board::getimCount(){
  return imCount;
}

bool Board::getOver(){
  return over;
}
