
CMP = g++ -std=c++14 -lX11
CLASS = board
MAIN = final
EXEC = pacman

$(EXEC): $(CLASS).o $(MAIN).o gfxnew.o
	$(CMP) $(CLASS).o $(MAIN).o gfxnew.o -o $(EXEC)

$(CLASS).o: $(CLASS).cpp $(CLASS).h
	$(CMP) -c -std=c++14 $(CLASS).cpp -o $(CLASS).o

$(MAIN).o: $(MAIN).cpp $(CLASS).h gfxnew.o
	$(CMP) -c -std=c++14 $(MAIN).cpp gfxnew.o -o $(MAIN).o

clean:
	rm $(MAIN).o
	rm $(EXEC)

