// Chau-Nhi Phan & Aemile Donoghue
// Section 05
// board.h Interface for the Board Class for the pacman game

class Board {
  public:
    Board();
    ~Board();
	
    bool move(char); // moves pacman to that position if get status returns true
    bool moveG(int, int); // direction, ghost
    char getStatus(int, int); // returns an int corresponding to what is located at a position
    void setStatus(int, int, char); // sets something to be located on a position. That thing is determined by the 3rd int passed
    void print();
    void setPac();
    void setG(int);
    int getPacX();
    int getPacY();
    int getPastPacX();
    int getPastPacY();
    void setPastPacX();
    void setPastPacY();
    void setPast(int, bool); // ghost 1-4
    int getDots();
    int getGX(int); // x position of ghost
    int getGY(int); // y position of ghost
    int getDomeX(int);
    int getDomeY();
    void setGpos(int, int, int); // row, col, ghost
    void drawMaze();
    void drawDome();
    void collision(int); // moving character
    void eat(int, int); // pacman, ghost
    void checkBothGhosts(int,int);
    bool getImmunity();
    void setImmunity(bool);
    void sendDome(int);
    void resetIm();
    int getimCount();
    bool getOver();
  private:
    char board[31][29];
    int pacrow, paccol; // pacman's current position
    int pastpacr, pastpacc; // pacman's previous position
    int grow[4], gcol[4]; // ghost coordinates
    bool past[4]; // true for '.', false for ' '
    int dots;
    bool immunity = false;
    int imCount;
    int domeX[4] = {16, 13, 14, 15}, domeY = 15;
    bool over;
};
